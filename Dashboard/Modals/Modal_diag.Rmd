---
title: "Modal_diag"
output: html_document
---


Le diagramme de flux ci-dessous met en évidence les <b>liens entre le type de passagers</b> (moitié gauche du diagramme) <b>et les notes globales</b> que ceux-ci ont attribuées à leur expérience aérienne (moitié droite)

Dans un souci de visibilité les notes ont été réparties en trois catégories :
* Bonnes Notes : entre 8/10 (inclus) et 10/10;
* Moyennes Notes : entre 5/10 (inclus) et 7/10;
* Mauvaises Notes : moins de 5/10.

En passant la souris sur un des flux, nous visualisons quelle quantité de passagers de ce type ont donné une note de cette catégorie.
Exemple: 

Screenshot

En passant la souris sur une des catégories, nous avons le nombre total de passagers du type choisi / le nombre total de notes dans cette catégorie.
Exemple: 

Screenshot

