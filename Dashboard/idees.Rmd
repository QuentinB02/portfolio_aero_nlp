---
title: "Idées"
output: html_document
---



# Recap so far

* Un flexdashboard adressé à AirFrance seulement
* Dans chaque onglet, tous les graphes doivent être déclinables selon classe éco / première class... Faire un onglet dédié à ce filtre, "Eco vs 1st"?  
* Un onglet NLP : WordCloud (global et in-cluster) + extraction de relations
* Un onglet avec des infos "géographiques" : chordDiagram entre les continents d'une part / les catégories de notes overall d'autre part (low/mid/high) + une carte montrant la répartition mondiale de telle ou telle variable + liste d'histo les mots les plus utilisés par continent
* Un onglet de comparaison avec d'autres compagnies particulières / le marché mondial



# Principales

* Word frequency : quels semblent être les mots qui reviennent le plus en général dans les reviews? Bien filtrer les stop-words et les mots trop fréquents (flights par ex)
* In-cluster frequency : En séparant les reviews positives / neutres / négatives en utilisant l'overall evaluation, quels sont les mots revenant le plus dans chacun des groupes?
* Relation extraction : quels mots ont le plus de chances d'apparaître ensemble et quelles leçons en tirer? Ex : même si "toilet" n'est pas un des mots les plus utilisés au vu du wordcloud, il peut être intéressant de constater qu'il apparaît preque systématiquement avec le mot "shit-smell"
* Répartition géographique des types de commentaires négatifs : les habitants de tel ou tel pays ne vont pas avoir les mêmes remarques négatives (ex : les Français vont trouver le cabin crew trop strict / rude, les Allemands pas assez)
* Qualité de la bouffe / boisson : sur quels trajets les repas sont-ils les meilleurs / les pires? ==> recommandation business concrète : revoir leur politique d'approvisionnement en bouffe 
* Afficher les stats de conforts des sièges, de ground service, entertainment, in-flight services, rapport Q/P, food and bev...
* Chord Diagram types de clients / satisfactions?



# Filtres

Pour chaque combinaison des filtres ci-dessous, display toutes les infos principales

* Airline
* Cabin type (eco / 1st)
* Passenger type (leisure / business)

Les deux derniers se recoupent un peu... Faire une catégorie commune pour les gens en première OU qui voyagent pour le business? Pas forcément idiot vu qu'en cumulant ces deux populations on doit bien être à 80-90% de la population qui achète des trucs en vol

Le filtre airline n'est pas forcément une bonne idée car il risque d'apporter trop de complexité dans le dashboard; il vaut peut-être mieux partir du principe que le produit est destinée à une compagnie aérienne particulière, et ne mentionner les autres qu'à titre de comparaison (AirFrance, à renommer pour l'occasion)

Après réflexion, on ne va utiliser que le filtre classe éco / première classe : certes, le contentement d'un client va impacter sa disposition à dépenser dans l'avion (si on est dans cette perspective), mais ce sont vraiment les conditions de vol qui vont influer sur son opinion globale de la compagnie


# Bonus

* Statistiques mondiales (aller pomper toutes les airlines sur Skytrax et envisager de scraper d'autres sites)
* Comparaison d'une compagnie donnée avec les "meilleures" compagnies au monde (critère à définir : classement externe à Skytrax)
* Essayer de sortir des corrélations entre date flown et overall quality (?)
* Corrélation entre les mots les plus fréquents et le recommended ou pas / les moins bonnes notes en services
* Chart Radar? https://www.datacamp.com/community/tutorials/sentiment-analysis-R



# Remarques

* Les clients qu'il faut le plus soigner sont ceux qui voyagent pour le business / en première classe (ne serait-ce que pour le in-flight spending)
* Essayer la TF-IDF plutôt que la fréquence?
