DROP TABLE IF EXISTS "airline_reviews"; 

CREATE TABLE airline_reviews (
    "airline"           VARCHAR(50) NOT NULL,  
    "route"             VARCHAR(150),  
    "overall"           INTEGER,  
    "cabin"             VARCHAR(50),  
    "traveller_type"    VARCHAR(50),  
    "customer_review"   VARCHAR(10000) NOT NULL,  
    "recommended"       BOOLEAN,  
    "author"            VARCHAR(50),  
    "date_flown"        VARCHAR(50),  
    "review_date"       DATE NOT NULL,  
    "value_for_money"   INTEGER,  
    "cabin_service"     INTEGER,  
    "entertainment"     INTEGER,  
    "seat_comfort"      INTEGER,  
    "aircraft"          VARCHAR(100),  
    "ground_service"    INTEGER,  
    "food_bev"          INTEGER
);
