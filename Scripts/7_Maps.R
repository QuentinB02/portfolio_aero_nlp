library(leaflet)
library(leafpop)
library(readr)
library(dplyr)
library(sf)
library(mapview)


# reviews = read_csv("Data/reviews_ok.csv")
# polygons = st_read("Data/countries_simple.geojson")
# continents = st_read("Data/continent_polygons_simple.geojson") 
# continents$continent = as.character(continents$continent)
# 
# continents = continents %>% 
#   arrange(continent) %>% 
#   filter(!(continent %in% c("Antarctica", "Australia")))

# for (i in 1:nrow(continents)) {
#   
#   continents$geometry[i] <- continents$geometry[i][[1]]
#   
# } # Fail : cast multipolygon to polygon?

# summary = reviews %>% 
#   filter(airline == "Air France" & !(is.na(continent_from))) %>% 
#   group_by(continent_from) %>% 
#   summarise(avg = mean(overall)) %>% 
#   arrange() 
# continents$avg = summary$avg

countries = readRDS("Dashboard/Data/countries_all_ok.rds")



# # Déja fait :
# countries$Overall_from_n = as.character(countries$Overall_from_n)
# countries$Overall_from = as.character(floor(countries$Overall_from * 100) / 100)
# countries$value_for_money_from_n = as.character(countries$value_for_money_from_n)
# countries$value_for_money_from = as.character(floor(countries$value_for_money_from * 200) / 100)
# countries$cabin_service_from_n = as.character(countries$cabin_service_from_n)
# countries$cabin_service_from = as.character(floor(countries$cabin_service_from * 200) / 100)
# countries$entertainment_from_n = as.character(countries$entertainment_from_n)
# countries$entertainment_from = as.character(floor(countries$entertainment_from * 200) / 100)
# countries$seat_comfort_from_n = as.character(countries$seat_comfort_from_n)
# countries$seat_comfort_from = as.character(floor(countries$seat_comfort_from * 200) / 100)
# countries$ground_service_from_n = as.character(countries$ground_service_from_n)
# countries$ground_service_from = as.character(floor(countries$ground_service_from * 200) / 100)
# countries$food_bev_from_n = as.character(countries$food_bev_from_n)
# countries$food_bev_from = as.character(floor(countries$food_bev_from * 200) / 100)
# countries$recommended_from_n = as.character(countries$recommended_from_n)
# countries$recommended_from = as.character(floor(countries$recommended_from * 1000) / 100)
# countries$popup = paste("<table>
#                                 <thead>
#                                     <tr>
#                                         <th colspan='3'>Statistiques sur :", countries$ADMIN, "</th>
#                                     </tr>
#                                 </thead>
#                                 <tbody>
#                                     <tr>
#                                         <td>Critère</td>
#                                         <td>Moyenne des notes</td>
#                                         <td>Quantité de notes</td>
#                                     </tr>
#                                     <tr>
#                                         <td>Note globale</td>
#                                         <td>", countries$Overall_from, "</td>
#                                         <td>", countries$Overall_from_n, "</td>
#                                     </tr>
#                                     <tr>
#                                         <td>Rapport Qualité / Prix</td>
#                                         <td>", countries$value_for_money_from, "</td>
#                                         <td>", countries$value_for_money_from_n, "</td>
#                                     </tr>
#                                     <tr>
#                                         <td>Service en cabine</td>
#                                         <td>", countries$cabin_service_from, "</td>
#                                         <td>", countries$cabin_service_from_n, "</td>
#                                     </tr>
#                                     <tr>
#                                         <td>Divertissement</td>
#                                         <td>", countries$entertainment_from, "</td>
#                                         <td>", countries$entertainment_from_n, "</td>
#                                     </tr>
#                                     <tr>
#                                         <td>Confort des sièges</td>
#                                         <td>", countries$seat_comfort_from, "</td>
#                                         <td>", countries$seat_comfort_from_n, "</td>
#                                     </tr>
#                                     <tr>
#                                         <td>Service au sol</td>
#                                         <td>", countries$ground_service_from, "</td>
#                                         <td>", countries$ground_service_from_n, "</td>
#                                     </tr>
#                                     <tr>
#                                         <td>Qualité des repas / boissons</td>
#                                         <td>", countries$food_bev_from, "</td>
#                                         <td>", countries$food_bev_from_n, "</td>
#                                     </tr>
#                                     <tr>
#                                         <td>Taux de recommandation</td>
#                                         <td>", countries$recommended_from, "</td>
#                                         <td>", countries$recommended_from_n, "</td>
#                                     </tr>
#                                 </tbody>
#                              </table>")
# 
# saveRDS(countries, "Dashboard/Data/countries_all_ok.rds")

# Leaflet tests
# leaflet(continents) %>% 
#   addTiles() %>% 
#   addPolygons(color = ~colorQuantile("YlOrRd", avg)(avg),
#               label = ~continent,
#               stroke = FALSE)
#   # addPolygons(fillColor = summary$avg, stroke = FALSE)



# Fail

# pal <- colorNumeric(
#   palette = "viridis",
#   domain = countries$Overall_from_n)
# 
# leaflet(data = countries) %>% 
#   addTiles() %>% 
#   addPolygons(fillColor = ~pal(Overall_from_n),
#               label = ~ADMIN,
#               stroke = FALSE,
#               group = "Overall",
#               highlightOptions = highlightOptions(color = "white", weight = 2,
#                                                   bringToFront = TRUE),
#               popup = ~ADMIN) %>% 
#   addLayersControl(baseGroups = c("Overall"))


# Mapview Tests (FAIL)
# mapview(countries, 
#         # zcol = c("Overall_from", "Overall_to", "value_for_money_from"),
#         burst = TRUE,
#         hide = TRUE) 
# 
# mapview(countries, zcol = "Overall_from") +
#   countries["Overall_to"]
# 
# mapview(countries["Overall_from_n"], label = make)







# Success : leaflet again

# Legend test
map = leaflet(countries) %>%
  # addTiles() %>% 
  addPolygons(label = ~ADMIN,                 ### Note Globale
              color = "#444444", 
              weight = 1, 
              smoothFactor = 0.5,
              opacity = 1.0, 
              fillOpacity = 0.5,
              fillColor = ~colorNumeric("viridis", as.numeric(Overall_from))(as.numeric(Overall_from)),
              highlightOptions = highlightOptions(color = "white", weight = 2,
                                                  bringToFront = TRUE),
              group = "Note globale",
              popup = ~popup) %>%
  addLegend(pal = colorNumeric("viridis", as.numeric(countries$Overall_from)),
            values = as.numeric(countries$Overall_from),
            position = "bottomright",
            title = "Note globale",
            labFormat = labelFormat(suffix = " / 10"),
            group = "Note globale") %>% 
  addPolygons(label = ~ADMIN,                 ### Rapport Qualité / Prix
              color = "#444444", 
              weight = 1, 
              smoothFactor = 0.5,
              opacity = 1.0, 
              fillOpacity = 0.5,
              fillColor = ~colorNumeric("viridis", as.numeric(value_for_money_from))(as.numeric(value_for_money_from)),
              highlightOptions = highlightOptions(color = "white", weight = 2,
                                                  bringToFront = TRUE),
              group = "Rapport Qualité / Prix",
              popup = ~popup) %>%
  addLegend(pal = colorNumeric("YlOrRd", as.numeric(countries$value_for_money_from)),
            values = as.numeric(countries$value_for_money_from),
            position = "bottomright",
            title = "Rapport Qualité / Prix",
            labFormat = labelFormat(suffix = " / 5"),
            group = "Rapport Qualité / Prix") %>% 
  addLayersControl(baseGroups = c("Note globale", 
                                  "Rapport Qualité / Prix"),
                   options = layersControlOptions(collapsed = FALSE)) %>% 
  setView(5, 40, 3) 

  
map
  

# Full map
explanation_popup = "Cette carte donne les valeurs moyennes par pays de chacune des variables numériques
disponibles dans les critiques, entre 0 et 10 / 10. Les pays sont considérés lorsqu'ils sont le point de 
DEPART d'un vol, jamais lorsqu'ils sont une destination. 
</br>
</br>
On a donc une vision de quel critère tient le plus à coeur aux passagers des différents pays, pour peu
que le nombre de revues soit suffisamment élevé. Ou alors on peut considérer que les compagnies opérant le
plus dans tel ou tel pays sont connues, et par conséquent que ces évaluations sont plus le reflet de la 
performance de la compagnie en question que d'une tendance de goût parmi les passagers. Il est évident que
des conclusions seraient plus faciles à tirer sur un jeu de données de taille plus importante.
</br>
</br>
Chaque critique n'évaluant pas forcément tous les champs, il nous a paru nécessaire de préciser dans la 
table récapitulative de chaque pays, accessible par un simple clic, le nombre d'avis considéré pour 
calculer les moyennes dans chaque champ.
</br>
</br>
Exemple en cliquant sur la France : sur l'ensemble des 122 revues au départ de France pour lesquelles 
la qualité du divertissement a été évaluée, la moyenne est de 6,27.
</br>
Si on avait plus de données, tri par compagnie visualisable"


map = leaflet(countries) %>%
  # addTiles() %>% 
  addPolygons(label = ~ADMIN,                 ### Note Globale
              color = "#444444", 
              weight = 1, 
              smoothFactor = 0.5,
              opacity = 1.0, 
              fillOpacity = 0.5,
              fillColor = ~colorNumeric("YlOrRd", as.numeric(Overall_from))(as.numeric(Overall_from)),
              highlightOptions = highlightOptions(color = "white", weight = 2,
                                                  bringToFront = TRUE),
              group = "Note globale",
              popup = ~popup) %>%
  addPolygons(label = ~ADMIN,                 ### Rapport Qualité / Prix
              color = "#444444", 
              weight = 1, 
              smoothFactor = 0.5,
              opacity = 1.0, 
              fillOpacity = 0.5,
              fillColor = ~colorNumeric("YlOrRd", as.numeric(value_for_money_from))(as.numeric(value_for_money_from)),
              highlightOptions = highlightOptions(color = "white", weight = 2,
                                                  bringToFront = TRUE),
              group = "Rapport Qualité / Prix",
              popup = ~popup) %>%
  addPolygons(label = ~ADMIN,                 ### Service en cabine
              color = "#444444", 
              weight = 1, 
              smoothFactor = 0.5,
              opacity = 1.0, 
              fillOpacity = 0.5,
              fillColor = ~colorNumeric("YlOrRd", as.numeric(cabin_service_from))(as.numeric(cabin_service_from)),
              highlightOptions = highlightOptions(color = "white", weight = 2,
                                                  bringToFront = TRUE),
              group = "Service en cabine",
              popup = ~popup) %>%
  addPolygons(label = ~ADMIN,                 ### Divertissement
              color = "#444444", 
              weight = 1, 
              smoothFactor = 0.5,
              opacity = 1.0, 
              fillOpacity = 0.5,
              fillColor = ~colorNumeric("YlOrRd", as.numeric(entertainment_from))(as.numeric(entertainment_from)),
              highlightOptions = highlightOptions(color = "white", weight = 2,
                                                  bringToFront = TRUE),
              group = "Divertissement",
              popup = ~popup) %>%
  addPolygons(label = ~ADMIN,                 ### Confort des sièges
              color = "#444444", 
              weight = 1, 
              smoothFactor = 0.5,
              opacity = 1.0, 
              fillOpacity = 0.5,
              fillColor = ~colorNumeric("YlOrRd", as.numeric(seat_comfort_from))(as.numeric(seat_comfort_from)),
              highlightOptions = highlightOptions(color = "white", weight = 2,
                                                  bringToFront = TRUE),
              group = "Confort des sièges",
              popup = ~popup) %>%
  addPolygons(label = ~ADMIN,                 ### Service au sol
              color = "#444444", 
              weight = 1, 
              smoothFactor = 0.5,
              opacity = 1.0, 
              fillOpacity = 0.5,
              fillColor = ~colorNumeric("YlOrRd", as.numeric(ground_service_from))(as.numeric(ground_service_from)),
              highlightOptions = highlightOptions(color = "white", weight = 2,
                                                  bringToFront = TRUE),
              group = "Service au sol",
              popup = ~popup) %>%
  addPolygons(label = ~ADMIN,                 ### Qualité des repas / boissons
              color = "#444444", 
              weight = 1, 
              smoothFactor = 0.5,
              opacity = 1.0, 
              fillOpacity = 0.5,
              fillColor = ~colorNumeric("YlOrRd", as.numeric(food_bev_from))(as.numeric(food_bev_from)),
              highlightOptions = highlightOptions(color = "white", weight = 2,
                                                  bringToFront = TRUE),
              group = "Qualité des repas / boissons",
              popup = ~popup) %>%
  addPolygons(label = ~ADMIN,                 ### Taux de recommandations
              color = "#444444", 
              weight = 1, 
              smoothFactor = 0.5,
              opacity = 1.0, 
              fillOpacity = 0.5,
              fillColor = ~colorNumeric("YlOrRd", as.numeric(recommended_from))(as.numeric(recommended_from)),
              highlightOptions = highlightOptions(color = "white", weight = 2,
                                                  bringToFront = TRUE),
              group = "Taux de recommandations",
              popup = ~popup) %>%
  addLayersControl(baseGroups = c("Note globale", 
                                  "Rapport Qualité / Prix",
                                  "Service en cabine",
                                  "Divertissement",
                                  "Confort des sièges",
                                  "Service au sol",
                                  "Qualité des repas / boissons",
                                  "Taux de recommandations"),
                   options = layersControlOptions(collapsed = FALSE)) %>% 
  addLegend(pal = colorNumeric("YlOrRd", as.numeric(countries$value_for_money_from)),
            values = as.numeric(countries$value_for_money_from),
            position = "bottomright",
            title = "Echelle de notation",
            opacity = 1,
            labFormat = labelFormat(suffix = " / 10")) %>% 
  # addLogo(position = "bottomleft", img = "fdti_logo.PNG") %>% 
  # addPopups(5, 10, 
  #           popup = explanation_popup,
  #           options = popupOptions(className = "custom_popup",
  #                                  minWidth = 700, 
  #                                  autoPan = FALSE)) %>% 
  setView(5, 40, 3)

map

# saveRDS(map, "Dashboard/Data/map.rds")

# Trop lourd... (15 MB) 

